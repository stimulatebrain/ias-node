var express = require('express'),
    app = express();

var fs = require('fs');
var cluster = require('cluster');
var sticky = require('sticky-session');
var http = require('http')
  , bodyParser = require('body-parser')
  , cors = require('cors');
var winston = require('winston');
var moment = require('moment');
var logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({
        timestamp: function(){
          return moment().format("YYYY-MM-DD HH:mm:ss.SSS");
        }, 
        colorize : true
      }) 
    ]
});

var formidable = require('formidable');
var path = require('path');


//핸들바 뷰 엔진 설정
var handlebars = require('express-handlebars')
    .create({
        defaultLayout: 'main',
        helpers: {
            trimString : function(passedString) {
                var theString = passedString.substring(0,19);
                return theString;
            },
        }
    });
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');
var viewPath = __dirname + '/views';


var port = 8060,
    num_processes = require('os').cpus().length;
var httpServer = http.createServer(app);
var io = sio(httpServer);
var redis = require("redis");


if (!sticky.listen(httpServer, port)) {
    // Master code
    httpServer.once('listening', function() {
      console.log('server started on 8060 port');
    });
    cluster.on('online', function(worker) {
        logger.info('Worker ' + worker.process.pid + ' is online');
    });
    cluster.on('exit', function(worker, code, signal) {
        logger.info('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        logger.info('Starting a new worker');
        cluster.fork();
    });
} else {
    // Worker code
    redisClient = redis.createClient();
        redisClient.on("error", function (err) {
    });

    //정적 파일 미들웨어
    app.use(express.static(__dirname + '/public'));

    //라우터
    app.get('/', function(req, res) {
        //views/layouts 폴더를 조회하여 리스트 저장
        var layouts = [],
            pages = [];
        layouts = fs.readdirSync(viewPath +'/layouts');
        for (var i in layouts) layouts[i] = layouts[i].substring(0, layouts[i].length-11);

        pages = fs.readdirSync(viewPath);
        for (var i in pages) if(pages[i].substring(0, pages[i].length-11).length !== 0) pages[i] = pages[i].substring(0, pages[i].length-11);
        res.render('index', {
            layouts: layouts,
            pages: pages
        });
    });

    app.use( bodyParser.urlencoded({ extended: true }) );

    app.use(bodyParser.json());
    app.post('/api/postPhoto', function(req,res){
         console.log('api postPhoto start');
         var form = new formidable.IncomingForm();
         form.parse(req, function(err,fields, files){
             var old_path = files.image.path,
                index = old_path.lastIndexOf('/') + 1,
                file_ext = files.image.name.split('.').pop(),
                file_name = old_path.substr(index),
                newFileName = moment().format('YYYYMMDD_HHmmss_SSS') + '.' + (file_ext == 'null' ? 'jpg' : file_ext );
                filePath = path.join(__dirname, '.', '/public/uploads/', newFileName);
            fs.readFile(old_path, function(err, data) {
                fs.writeFile(filePath, data, function(err) {
                    fs.unlink(old_path, function(err) {
                        redisClient.incr("photo:count", function(err,reply){
                                var object =   {"seq" : reply , 
                                   "name" : fields.name,
                                   "seatNo" : fields.seatNo ,
                                   "insertDate" : moment().format('YYYY-MM-DD HH:mm:ss:SSS Z'),
                                   "fileName" : newFileName };
                                redisClient.hmset('photo:' + reply, object);
                                redisClient.hgetall('currentPerformance', function(err, obj){
                                    redisClient.sadd('photos:' + obj.seq, 'photo:' + reply);
                                });
                        });
                        res.status(200);
                        res.json(getResultObject(100, '성공', {'imgUrl' : 'http://211.249.63.207:8060/uploads/' + newFileName}) );
                    });
                });
            });
         });
    });

    app.get('/api/currentPerformance', function(req,res){
        redisClient.hgetall('currentPerformance', function(err,obj){
            res.status(200).send({currentPerformanceSeq : obj.seq});
        });
    });

    app.get('/api/vote', function(req,res){
       redisClient.hgetall('currentPerformance', function(err,obj){
            redisClient.hincrby('votes:' + obj.seq, req.query.number, 1);
            res.send('success');
        });
    });

  
    app.get('/admin/restartWorker', function(req,res){
     var wid, workerIds = [];
      for(wid in cluster.workers) {
          workerIds.push(wid);
      }
      workerIds.forEach(function(wid) {
          cluster.workers[wid].send({
              text: 'shutdown',
              from: 'master'
          });

          setTimeout(function() {
              if(cluster.workers[wid]) {
                  cluster.workers[wid].kill('SIGKILL');
              }
          }, 5000);
      });
      res.send(getResultObject(100, '성공' , ''));
    });


    //익스프레스 라우터 설정
    var admin = require('./routes/admin')(app, redisClient)
        user = require('./routes/user');

    app.use('/', user);
    //포트 설정
    //404
    app.use(function(req, res, next) {
        res.status(404);
        res.send('404 - Not Found');
    });

    function getResultObject (resultCode, resultMessage, resultObject) {
      return { 'resultCode' : resultCode, 'resultMessage' : resultMessage, 
                   'resultObject' : resultObject };
    }
}



// app.listen(app.get('port'), function(){
//     console.log('Express started on http://localhost:' + app.get('port') +';');
// });
