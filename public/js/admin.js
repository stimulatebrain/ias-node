//모바일 메뉴 아이콘에 이벤트 리스너 설정
$('#menuButton').on('click', function(){
    if($('nav').hasClass('active')) {
        $('nav').removeClass('active');
    } else {
        $('nav').addClass('active');
    }
});

//현재 공연 변경 버튼에 이벤트 리스너 설정
$('#change').on('click', function(e){
    if ($('#selecting').hasClass('active')) {
        $('#selecting').removeClass('active');
    } else {
        $('#selecting').addClass('active');
    }
});
