var modal = function(param){
var socket = io.connect('http://211.249.63.207:8060');

$(document).ready(function(){
	socket.on('xSync:admin:startProgram', function(data){
	    eventType = data.resultObject.info.eventType;
	    level = data.resultObject.info.level;
	    g("startProgram", eventType, level);
	});
	socket.on('xSync:admin:endProgram', function(data){
	    eventType = data.resultObject.info.eventType;
	    level = data.resultObject.info.level;
	    g("endProgram", eventType, level);
	});
	$("button.buttons").click(function(){
	    //$(this).attr("disabled", "disabled");

	    if($(this).data("method") == "startProgram"){
	        if( $(this).data("event-type") == 'photo'){
	            startProgramForPhoto($(this).data('event-type'), $(this).data('level'), $("#photoUrl").val());
	        }else {
	            startProgram($(this).data('event-type'), $(this).data('level'),
	                        $(this).data('screen-mode'), $(this).data('color'),
	                        $(this).data('delay'), $(this).data('cardmode'), 
	                        $(this).data('interval'), $(this).data('url'));
	        }
	    }else if($(this).data("method") == "endProgram"){
	        endProgram($(this).data('event-type'), $(this).data('level'));
	    }

	});
});

function startProgram(eventType, seq, screenMode, color, delay, cardMode, interval, url){
    if ( color == null) color = '';
    if ( screenMode == null) screenMode = '';
    if ( delay == null) delay = '';
    if ( cardMode == null) cardMode = '';
    if ( interval == null) interval = '';
    if ( url == null ) url = '';

    socket.emit('xSync:admin:startProgram' , {'eventType' : eventType, 'seq': seq,
                                                    'screenMode' : screenMode, 'color' : color,
                                                    'delay' : delay, 'cardMode' : cardMode, 'interval' : interval, 
                                                    'url' : url});
}
function startProgramForPhoto(eventType, seq, url){
    socket.emit('xSync:admin:startProgram' , {'eventType' : eventType, 'seq': seq, 'url' : url});
}
function endProgram(eventType, seq){
    socket.emit('xSync:admin:endProgram' , {'eventType' : eventType, 'seq': seq});
}


function g(m, e, l){
    $("button").removeClass("active");
    b = $("button[data-method="+m+"][data-event-type="+e+"][data-level="+l+"]");
    b.addClass("active");
    b.removeAttr("disabled");
}

function g_notactive(m, e, l){
    $("button").removeClass("active");
    b = $("button[data-method="+m+"][data-event-type="+e+"][data-level="+l+"]");
    b.removeAttr("disabled");
}
