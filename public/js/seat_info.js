/**
 * seat_info.js
 *
 * 좌석 선택 페이지에서 동작하는 자바스크립트를 정의한다.
 */

$(document).ready(function() {
	// '입력완료' 버튼에 리스너를 설정한다.
	$("#btn_seat_confirm").click(function(e) {
        var seatSection = $('#seat_section').val(),
        seatRow = $('#seat_row').val(),
        seatNumber = $('#seat_number').val();

   
        if( !checkSeatValid(seatSection, seatRow, seatNumber) ){
            modal('seatWarning');
            return;
        }

        

		// 카드섹션 서버로 좌석정보 저장한다.
		var seatNo = seatSection + ':' + seatRow + ':' + seatNumber;

		insertUserSeatInfo(seatNo, function(){
			location.href = '/seat/seat_confirm';
		});
		completeIntro();
	});

	// '좌석번호 다시입력' 버튼에 리스너를 설정한다.
	$('#btn_reinsert').click(function(){
		// 좌석번호 입력 페이지로 이동한다.
		location.href = "/seat/seat_input";
	});
	
	// '다음' 버튼에 리스너를 설정한다.
	$('#btn_confirm').click(function(){
		// 좌석번호 입력 페이지로 이동한다.
		location.href = "/main/intro";
	});
 
	$("#seat_section").change(function(){
		var value = $(this).val();
		var dataArr = [];
		$("#seat_row option").remove();

		if(value == 'A' || value == 'B' || value == 'C' || value == 'D'){
			for (var i = 1; i < 23 ; i++){
				dataArr.push(i + '');
			}
		}else if(value =='E' || value =='F' || value =='G' || value =='H' ){
			for (var i = 1; i < 15 ; i++){
				dataArr.push(i + '');
			}
		}else {
			for (var i = 1; i < 6 ; i++){
				dataArr.push(i + '');
			}
		}
		
		$("#seat_row").append('<option value=""></option>');
		for(var i=0; i< dataArr.length; i++){
			$("#seat_row").append($("<option value='" + dataArr[i] + "'>" + dataArr[i] +"</option>"));
		}
	});

	$("#btn_intro_confirm").click(function(){
		location.href = '/seat/seat_input';
	});
});

function getSeatNo(){
	console.log('getSeatNo!');
	getData('seatNo', function(r){
		$("#seat_no").html(r);
	});
}
function checkSeatValid(seatSection, seatRow, seatNumber){
	if( !seatSection || seatSection == ''|| !seatRow || seatRow == '' || !seatNumber || seatNumber == '' ) return false;
	return true;
}

xsyncInit();
