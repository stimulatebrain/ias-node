
var isAndroid = getMobileOperatingSystem() == "Android";
var isIOS = getMobileOperatingSystem() == "iOS";
var bridge = null;
var uId = null;
var userSeq = null;
var status = null;
var seatNo = null;

function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
        return 'iOS';
    }
    else if (userAgent.match(/Android/i)) {
        return 'Android';
    }
    else {
        return 'unknown';
    }
}
function connectIOSBridge(callback) {
    if (window.WebViewJavascriptBridge) {
        console.log('WebViewJavascriptBridge Exist');
        callback(WebViewJavascriptBridge)
    } else {
        console.log('WebViewJavascriptBridge Doesnt Exist');
        document.addEventListener('WebViewJavascriptBridgeReady', function() {
            console.log('WebViewJavascriptBridge Add Event Complete');
            callback(WebViewJavascriptBridge)
        }, false)
    }
}
String.form = function(str, arr) {
    var i = -1;
    function callback(exp, p0, p1, p2, p3, p4) {
        if (exp=='%%') return '%';
        if (arr[++i]===undefined) return undefined;
        var exp  = p2 ? parseInt(p2.substr(1)) : undefined;
        var base = p3 ? parseInt(p3.substr(1)) : undefined;
        var val;
        switch (p4) {
            case 's': val = arr[i]; break;
            case 'c': val = arr[i][0]; break;
            case 'f': val = parseFloat(arr[i]).toFixed(exp); break;
            case 'p': val = parseFloat(arr[i]).toPrecision(exp); break;
            case 'e': val = parseFloat(arr[i]).toExponential(exp); break;
            case 'x': val = parseInt(arr[i]).toString(base?base:16); break;
            case 'd': val = parseFloat(parseInt(arr[i], base?base:10).toPrecision(exp)).toFixed(0); break;
        }
        val = typeof(val)=='object' ? JSON.stringify(val) : val.toString(base);
        var sz = parseInt(p1); /* padding size */
        var ch = p1 && p1[0]=='0' ? '0' : ' '; /* isnull? */
        while (val.length<sz) val = p0 !== undefined ? val+ch : ch+val; /* isminus? */
        return val;
    }
    var regex = /%(-)?(0?[0-9]+)?([.][0-9]+)?([#][0-9]+)?([scfpexd])/g;
    return str.replace(regex, callback);
};
String.prototype.$ = function() {
    return String.form(this, Array.prototype.slice.call(arguments));
};

function addQSParm(url, name, value) {
    var re = new RegExp("([?&]" + name + "=)[^&]+", "");

    function add(sep) {
        url += sep + name + "=" + encodeURIComponent(value);
    }

    function change() {
        url = url.replace(re, "$1" + encodeURIComponent(value));
    }
    if (url.indexOf("?") === -1) {
        add("?");
    } else {
        if (re.test(url)) {
            change();
        } else {
            add("&");
        }
    }
    return url;
}



function insertUserID(uid){
    socketEmit("xSync:general:insertUserId" , {'uid' : uid}, function(data){
        if(data.resultCode == 100){
            uId = data.resultObject.uid;
            userSeq = data.resultObject.userSeq;
            setUserSeq(userSeq);
        }
    })
}


function insertUserInfo(_uId, _seatNo, _pushToken, _completeFunc){
    socketEmit("xSync:general:insertUserId" , 
                {'uid' : _uId, 'seatNo' : _seatNo, 
                 'pushToken' : _pushToken, 
                 'devicePlatform' : isIOS ? 'iOS' : 'Android'}, 
                function(data){
        if(data.resultCode == 100){
            userSeq = data.resultObject.userSeq;
            setUserSeq(userSeq);
            setData("seatNo", _seatNo, function(r){
                _completeFunc();
            });
        }
    })
}
function getCurrentStatus(userSeq, callback){
    socketEmit('xSync:general:getCurrentStatus' , {'userSeq' : userSeq}, callback);
}

function getCurrentStatusWithUUID(userSeq, uid, callback){
    socketEmit('xSync:general:getCurrentStatus' , {'userSeq' : userSeq, 'uid' : uid}, callback);
}
function insertQuizResult(quizSeq, answer, userSeq){
    socketEmit('xSync:quiz:insertQuizResult' , {'quizSeq' : quizSeq, 'answer': answer, 'userSeq': userSeq}, function(data){
    })
}
function insertVoteResult(voteSeq, answer, userSeq){
    socketEmit('xSync:vote:insertVoteResult' , {'voteSeq' : voteSeq, 'answer': answer, 'userSeq': userSeq}, function(data){
    });
}
function getLiteralData(userSeq, cardSeq, interval, cardMode){
    socketEmit('xSync:cardSection:getLiteralData' , {'userSeq': userSeq, 'seq' : cardSeq, 'interval' : interval, 'cardMode' : cardMode}, function(data){
    });
}

function completeVote(callback){
    callHandler('completeVote', {}, callback);
}

function completeIntro(callback){
    callHandler('completeIntro', {}, callback);
}

function getLiteralDataForTest(userSeq){
    socketEmit('xSync:cardSection:getLiteralDataForTest' , {'userSeq': userSeq}, function(data){
    });
}
function callHandler(method, params, callback){
    console.log('callHandler ' + method);
    bridge.callHandler(method, params, callback);
}

function setData(k, v, callback){
    callHandler('setData', {'key': k, 'value': v}, callback);
}
function getData(k, callback){
    callHandler('getData', {'key': k}, callback);
}
function getUID(callback){
    callHandler('getUID', {}, callback);
}
function setUserSeq(seq, callback){
    setData('userSeq', seq, callback);
}
function getUserSeq(callback){
    getData('userSeq', callback);
}
function getTime(callback){
    callHandler('getTime', {}, callback);
}
function startCardSection(callback){
    callHandler('startCardSection', {}, callback);
}
function socketEmit(method, params, callback){
    bridge.callHandler('requestSocket', {'methodName': method, 'params': params}, callback);
}
function socketOn(method, callback){
    bridge.registerHandler(method, function(data, responseCallback) {
        callback(data);
    })
}

function xsyncInit() {
    if( isIOS || isAndroid){
        connectIOSBridge(function (b) {
            console.log('Bridge start ' + b);
            b.init(function (message, responseCallback) {
            });
            bridge = b;
            bridgeConnected();
            getUID(function (r) {
                uId = r;
                console.log('uid is ' + r);
                //getUserSeq(function (r) {
                //    userSeq = r;
                //    if (userSeq == null) {
                //        insertUserID(uId);
                //    }else{
                //        // checkStatus();
                //    }
                //
                //});
            });
        });

        // if (isIOS){
        //     console = new Object();
        //     console.log = function(log) {
        //       var iframe = document.createElement("IFRAME");
        //       iframe.setAttribute("src", "ios-log:#iOS#" + log);
        //       document.documentElement.appendChild(iframe);
        //       iframe.parentNode.removeChild(iframe);
        //       iframe = null;    
        //     };
        //     console.debug = console.log;
        //     console.info = console.log;
        //     console.warn = console.log;
        //     console.error = console.log;
        //     console.log('Initial Complete');
        // } 
    }
}
var checkStatus = function(){
};
var bridgeConnected = function(){
};


function insertUserSeatInfo(seatNumber, completeFunc){
    getData('xSync_pushToken', function(r){
         insertUserInfo(uId, seatNumber, r, completeFunc);
    });
}



