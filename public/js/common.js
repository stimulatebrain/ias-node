var modal = function(param, callback){
    //기본 모달 콘텐츠
    var contents = {
        dataWarning: '네트워크 연결이 불안정합니다.<br/>와이파이 혹은 데이터 네트워크를<br/>확인해주세요.',
        seatWarning: '좌석번호를 올바르게 입력해주세요.',
        omitWarning: '결과를 선택해주세요.',
        voteWarning: '투표가 제출되었습니다.<br/>감사합니다.'
    };

    //모달 콘텐츠 검색
    var content;
    for (var name in contents) {
        if(name === param) {
            content = contents[name]
            break;
        }
        content = '해당 데이터 없음';
    }

    //모달 콘텐츠 포맷
    var send = '<div class="modal_bg">'
            + '<div class="modal_box">'
                + '<div class="modal_text">'
                    + '<div class="modal_text_inner">'
                        + content
                    + '</div>'
                + '</div>'
                + '<div class="modal_buttons">'
                    + '<button type="cancle" name="button">확인</button>'
                + '</div>'
            + '</div>'
        + '</div>';

    //모달을 문서에 삽입한다
    $('body').append(send);
    $('.modal_buttons button').on('click', function(){
        $('.modal_bg').remove();
        callback();
    });
};
