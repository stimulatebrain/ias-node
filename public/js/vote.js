/**
 * seat_info.js
 *
 * 좌석 선택 페이지에서 동작하는 자바스크립트를 정의한다.
 */

$(document).ready(function() {

	$('#vote_complete').click(function(){
		var checked_item = $(':radio[name="candidate"]:checked').val();
		if( checked_item == undefined) {
			modal('omitWarning');
			return;
		}
		$.get('/api/vote', {'number' : checked_item}, 
			function(){
				modal('voteWarning', function(){
					completeVote(function(){
					});		
				});	
		});
	});
});


xsyncInit();
