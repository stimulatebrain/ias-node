module.exports = function(app, redisClient){

	var passport = require('passport');
	var LocalStrategy = require('passport-local').Strategy;
	var session = require('express-session')
	var redisStore = require('connect-redis')(session);
	var async = require('async');

	app.use(session(
	    {
	        secret: 'secret_key',
	        store: new redisStore({
	            host: "127.0.0.1",
	            port: 6379,
	            client: redisClient,
	            prefix : "session:",
	            db : 0
	        }),
	        saveUninitialized: false, // don't create session until something stored,
	        resave: true // don't save session if unmodified
	    }
	));

    app.use(passport.initialize());
    app.use(passport.session());

    passport.serializeUser(function(user, done) {
	  done(null, user);
	});

	passport.deserializeUser(function(user, done) {
	  done(null, user);
	});

	passport.use(new LocalStrategy({
	        usernameField: 'id',
	        passwordField: 'password'
	      },
	      function(email, password, done) {
	      // 인증 정보 체크 로직
	        if (email === 'admin-cultwo' && password === 'xsync@123') {
	          var user = {id: '1'};
	          return done(null, user);
	        } else {
	          return done(null, false, { message: 'Fail to login.' });
	        }
	      }
	 ));

	function ensureAuthenticated(req, res, next) {
	    // 로그인이 되어 있으면, 다음 파이프라인으로 진행
	    if (req.isAuthenticated()) { return next(); }
	    // 로그인이 안되어 있으면, login 페이지로 진행
	    res.redirect('/admin');
	}

	app.get('/admin', function(req, res){
		if( req.isAuthenticated()){
			res.redirect('/admin/control');
		}else {
		    res.render('admin/login', {layout: null});
		}
	});

	app.post('/admin', passport.authenticate('local', {
			successRedirect : '/admin/control',
			failureRedirect : '/admin',
			failureFlash : false
		})
	);

	app.get('/admin/logout', function(req,res){
	   req.logout();
	   res.redirect('/admin');
	});

	app.get('/admin/changePerformance', function(req,res){
		var seq = req.query.seq;
		var name = req.query.name;
		redisClient.set('test', 'test123');
		var object =  {
                   "seq" : seq,
                   "name" : name, 
               };
		redisClient.hmset('currentPerformance', object);
		res.redirect('/admin/control');
	});

	

	app.get('/admin/:name', ensureAuthenticated, function(req, res){
		redisClient.hgetall('currentPerformance', function(err, obj){
			var object = {layout: 'admin', currentPerformance : obj};
			if( req.params.name == 'control'){
				redisClient.scard('performance:' + obj.seq + ':users', function(err,obj){
					object.currentUser = obj;
					res.render('admin/'+req.params.name, object);
				});
			}else if( req.params.name == 'photos' ){
				redisClient.smembers('photos:' + object.currentPerformance.seq, function(err,obj){    		
					var photoObjs = [];
					var number = 1;
					async.each( obj, function ( photoId, callback){
						redisClient.hgetall(photoId, function(err, photoObj){
							photoObjs.push(photoObj);
							callback();
						});
					}, 
		    		function (err)
		    		{
		    			if( err) {
		    				console.log(err);
		    			}
		    			photoObjs.sort(function(a,b){
		    				return new Date(a.insertDate) - new Date(b.insertDate);
		    			});
		    			for(var i=0; i<photoObjs.length; i++){
		    				photoObjs[i].number = (i+1);
		    			}
		    			object.photos = photoObjs;
						res.render('admin/'+req.params.name, object);
		    		});
				});
			}else if( req.params.name == 'result'){
				redisClient.hgetall('votes:' + object.currentPerformance.seq, function(err, obj){
					if ( obj == null) {
						res.render('admin/' + req.params.name, object);
						return;
					}
					var number1 = isNaN(Number(obj['1'])) ? 0 : Number(obj['1']);
					var number2 = isNaN(Number(obj['2'])) ? 0 : Number(obj['2']);
					var number3 = isNaN(Number(obj['3'])) ? 0 : Number(obj['3']);
					var number4 = isNaN(Number(obj['4'])) ? 0 : Number(obj['4']);
					var number5 = isNaN(Number(obj['5'])) ? 0 : Number(obj['5']);
					var number6 = isNaN(Number(obj['6'])) ? 0 : Number(obj['6']);
					
					var overall = number1 + number2 + number3 + number4 + number5 + number6;
					var results = [];
					results.push({number : 1, count: number1, percent: (number1 / overall * 100) + '%'});
					results.push({number : 2, count: number2, percent: (number2 / overall * 100) + '%'});
					results.push({number : 3, count: number3, percent: (number3 / overall * 100) + '%'});
					results.push({number : 4, count: number4, percent: (number4 / overall * 100) + '%'});
					results.push({number : 5, count: number5, percent: (number5 / overall * 100) + '%'});
					results.push({number : 6, count: number6, percent: (number6 / overall * 100) + '%'});

					object.results = results;
					res.render('admin/'+req.params.name, object);
				});

			}else {
				res.render('admin/'+req.params.name, object);
			}
		});
	});
}
