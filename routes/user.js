var express = require('express');
var router = express.Router();
var fs = require('fs');
var viewPath = __dirname + '/../views';

router.get('/', function(req, res) {
    //views/layouts 폴더를 조회하여 리스트 저장
    var layouts = [],
        pages = [];
    layouts = fs.readdirSync(viewPath + '/layouts');
    for (var i in layouts) layouts[i] = layouts[i].substring(0, layouts[i].length-11);

    pages = fs.readdirSync(viewPath + '/user');
    for (var i in pages) if(pages[i].substring(0, pages[i].length-11).length !== 0) pages[i] = pages[i].substring(0, pages[i].length-11);
    res.render('user/index', {
        layouts: layouts,
        pages: pages
    });
});

router.get('/:layout/:name', function(req, res) {
    //URL 파라미터를 조회하여 저장
    var name = req.params.name,
        layout = req.params.layout;

    //views/layouts 폴더를 조회하여 리스트 저장
    var layouts = [];
    var data = fs.readdirSync(viewPath +'/layouts');
    for (var i in data) {
        layouts[i] = data[i].substring(0, data[i].length-11);
    }
    //파라미터 값이 리스트에 있는 지 판별, 없을 시 에러화면 표시
    if (layouts.indexOf(layout) == -1) {
        res.send('올바른 레이아웃을 입력해주세요.');
    } else {
        res.render('user/'+name, {
            layout: layout,
            name: name
        }); // 콘텍스트 객체 전달이 필요할 경우 switch문 사용 가능
    }
});

module.exports = router;
